<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// |微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use app\BaseController;
use think\facade\View;
use think\facade\Config;
use think\facade\Db;
use think\facade\Request;
use ectlog\Log;

/**
 * 后台视图控制器
 * 作者微信：N79823
 * 官网：https://pmhapp.com
**/

// require_once APP_PATH . 'ectools/Log.php'; 

class EctAdmin extends Base
{   
    public function ceshi(){
         dump(is_SSL());
    }
    
    public function apptype(){
        return 0;
    }
    
    /**
     * 后台入口文件
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/
    public function index(){
      return View::fetch("Admin/ect_admin/index");
    }
    
    /**
     * 后台首页
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/
    public function duindex(){
      $web_config = Db::name('config')->where('key','web_config')->find();
      
      if(!empty($web_config)){
         $data = json_decode($web_config['value'],true);
         
      }else{
         $data = ''; 
      }
      
      $empty = '<b style="color:rgb(30, 159, 255);">请前往先 网站管理-网站设置-网站配置 内配置信息</b>';
      
      View::assign('empty',$empty);
      View::assign('data',$data);
             
      return View::fetch("Admin/ect_admin/duindex");  
    }
    
    /**
     * 系统配置
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/    
    
    public function set_system(){
        
      $show_error_msg = Config::get('app.show_error_msg');
      View::assign('show_error_msg',$show_error_msg); 
      
      $close = Config::get('log.close');
      View::assign('close',$close); 
      
      $zsmb = Config::get('app.zsmb');
      View::assign('zsmb',$zsmb); 
      
      $ect_log = Config::get('app.ect_log');
      View::assign('ect_log',$ect_log); 
      
      $ect_debug_mode = Config::get('app.ect_debug_mode');
      View::assign('ect_debug_mode',$ect_debug_mode); 
      
      return View::fetch('Admin/ect_admin/set/set_system');  
    }
    
    /**
     * 网站配置
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/     
    public function set_web(){
      $web_config = Db::name('config')->where('key','web_config')->find();
      
      $data = json_decode($web_config['value'],true);
      //dump($web_config);
      View::assign('data',$data);
      return View::fetch('Admin/ect_admin/set/set_web');   
    }
    
    /**
     * 面板助手
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $text @内容显示
    **/     
    public function zsmb(){
      $text = input('get.text');
      View::assign('text',$text);  
      return View::fetch('Admin/ect_admin/public/zsmb');   
    }  
    
    /**
     * 站点日志
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/       
    public function ect_log(){
       //View::assign();
       return View::fetch('Admin/ect_admin/log/ect_log');
    }
    
    /**
     * 站点日志详情页
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/    
    public function ect_log_page(){
        
       $id = input('get.id');
       $data = Db::name('log')->where(['id'=>$id])->find();
       
       $place_zh = TaoBaoIpApi . $data['place'] . TaoBaoIpApi_accessKey;
       $place_zh = geturl($place_zh);
       $data['place_zh'] = $place_zh['data']['city'];;

       View::assign('data',$data);
       return View::fetch('Admin/ect_admin/log/ect_log_page');
    }
    
    /**
     * 系统信息
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/     
    public function ect_system_info(){
        
       $system_info = system_info();
        
       View::assign('system_info',$system_info);
       return View::fetch('Admin/ect_admin/info/ect_system_info'); 
    }
    
    /**
     * 程序开发list页面
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $_file @app存放路径
     * $app_list @目录下所有文件夹
     * $b @提取文件夹目录下所有文件 key=>value key目录名 value文件名
     * 
    **/
    public function program(){
        
      //获取App程序
      $app_fonfig = EctApp(); 
            
      $default_controller = Config::get('route.default_controller');
      $default_controller = explode('/', $default_controller); 
      
      if(count($default_controller)>1){
          $default_controller = $default_controller[0];
      }else{
         $default_controller = 'index'; 
      }
      
      View::assign('default_controller',$default_controller);
      View::assign('app',$app_fonfig);
      
      //var_dump($app_fonfig); 
      return View::fetch('Admin/ect_admin/app/program'); 
    }
    
    /**
     * CONTROLLER::开发代码界面-fu
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_name @目录名
     * $type @属性
     * $file @完整目录路径
     * $files @目录下所有文件名
    **/  
    public function ect_code($file_name){
        
      $file = APP_PATH . 'ectools_app/' . $file_name;
      $files = getDirContent($file);
      // dump($files);
      //dump($file_name);
      View::assign('file',$file);
      View::assign('files',$files);
      
      View::assign('file_name',$file_name);
      //dump($file_name);
      View::assign('type',1);
      return View::fetch('Admin/ect_admin/app/ect_code');  
    }
    
        
    /**
     * VIEW::开发代码界面-fu
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_name @目录名
     * $type @属性
     * $file @完整目录路径
     * $files @目录下所有文件名
    **/     
    public function ect_code_view($file_name){
      $file = APP_PATH . 'ectools_view/' . $file_name;
      $files = getDirContent($file);
     // dump($file);
     //   dump($file_name);
      View::assign('file',$file);
      View::assign('files',$files);
      View::assign('file_name',$file_name);
      View::assign('type',2);
      return View::fetch('Admin/ect_admin/app/ect_code');  
    }
    
    public function ect_code_extend(){
      $file =  input('get.file_name');
      //dump($file);
      $files = getDirContent($file);
     // dump($file);
      View::assign('file',$file);
      View::assign('files',$files);
    //   View::assign('file_name',$file_name);
      View::assign('type',3);
      return View::fetch('Admin/ect_admin/app/ect_code');  
    }
    
    /**
     * 开发代码详情页-zi
     * 
     * 作者微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file @文件名 index.php
     * $file_path @完整目录路径 /www/wwwroot/ectools.com/tp/public/../app/ectool/wx
     * $file_1 @文件后缀 json
     * $files @路径名  /www/wwwroot/ectools.com/tp/app/ectool/wx
    **/    
    public function ect_code_page(){
    
       $file = input('get.file');
       
       
       // 渲染文件名

       View::assign('file',$file);
        
        if(input('get.type') == 3){
           $ssl = is_SSL()?0:1; 
           View::assign('ssl',$ssl);
          
           $file_path = input('get.file_path');
           //dump($file_path);
           
           // 2022 4 23 优化 组合文件位置
           $file_name = input('get.file_name');
           // 
           $routes =  $file;
           //$file_path = app()->getRootPath();
           // dump(APP_PATH);
           // dump($file_path);
           $route = $file;
                  
           if(!empty($file)){
               $file_1 = file_1($file);
               //dump($file);
               $file = $file_path . '/' . $file;
               $file_get_contents = file_get_contents($file);//读取内容
               $route = file_0($route);
               
               View::assign('route',$route);
               View::assign('file_1',$file_1);
               // 保存路径名
               View::assign('file_path_save',$file);
               View::assign('file_path',$file);
               View::assign('file_get_contents',trim($file_get_contents));
           }else{
               View::assign('file_path',$file = '');
           }
            
        }else{
          $ssl = is_SSL()?0:1; 
          View::assign('ssl',$ssl);
          
           $file_path = input('get.file_path');
           //dump($file_path);
           
           // 2022 4 23 优化 组合文件位置
           $file_name = input('get.file_name');
           // 
           $routes =  $file;
           //$file_path = app()->getRootPath();
           // dump(APP_PATH);
           // dump($file_path);
           $route = $file;
                  
           if(!empty($file)){
               $file_1 = file_1($file);
               //dump($file);
               $file = $file_path . '/' . $file;
               $file_get_contents = file_get_contents($file);//读取内容
               $route = file_0($route);
               
               View::assign('route',$route);
               if($file_1 == 'htm' || $file_1 == 'html'){
                $files =  RootPath() . 'app/ectools_view/' . $file_name;
                View::assign('file_1',$file_1.'l');   
               }else{
                $files =  RootPath() . 'app/ectools_app/' . $file_name;
                View::assign('file_1',$file_1);
               }
               
               // 路径名 + 文件名
               $files = $files . '/' . $routes;
                
               // 保存路径名
               View::assign('file_path_save',$files);
               View::assign('file_path',$files);
               View::assign('file_get_contents',trim($file_get_contents));
           }else{
               View::assign('file_path',$file = '');
           }
        }
       
       return View::fetch('Admin/ect_admin/app/ect_code_page');  
    }
    
    public function new_app(){
    
       return View::fetch('Admin/ect_admin/app/new_app');      
    }
    
    public function new_file($file_name,$type){
        //exit();
       View::assign('file_name',$file_name);
       View::assign('type',$type);
       return View::fetch('Admin/ect_admin/app/new_file');      
    }
        
    public function EctCode(){
        $ssl = '';
        
        if(is_SSL()){
            $ssl = 'https://';
        }else{
           $ssl = 'http://'; 
        }
        
        
       $token = Db::name('config')->where('key','ectToken')->find();
        //dump($token);
        if($token){
            $token = $token['value'];
        }else{
            $token = "";
        }
       $url = $ssl . $_SERVER['HTTP_HOST'] . '/';
       View::assign('url',$url); 
       View::assign('token',$token); 
       return View::fetch('Admin/ect_admin/app/ectCode');       
    }
    
    public function EctPlugin(){
        
        $data = getFolderList(dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/');
        $extend_data = [];
        foreach ($data as $k){
            $file = dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/' . $k . '/ect.plugin.config.json';
            if(file_exists($file) && is_json($file)){

                    $json = json_decode(file_get_contents($file),true);
                    
                    $json['route'] = dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/' . $k;
                    if(isset($json['plugin_id'])){
                        // dump($json['plugin_id']);
                        array_push($extend_data,$json);
                    }
            }
        }
        
        View::assign('extend_data',$extend_data); 
        
        // 读取配置文件
        
        return View::fetch('Admin/ect_admin/app/ect_plugin_list');
    }


    public function EctApp(){
        
        return View::fetch('Admin/ect_admin/app/ect_app_list');
    }
}