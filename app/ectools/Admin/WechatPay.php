<?php

namespace app\ectools_app;
// 微信支付接口

use think\facade\View;
use think\Template;
use think\facade\Session;
use think\facade\Request;
use think\facade\Db;

require_once APP_PATH . 'common/wxpay/lib/WxPay.Api.php';


// use GuzzleHttp\Exception\RequestException;
// use WechatPay\GuzzleMiddleware\WechatPayMiddleware;
// use WechatPay\GuzzleMiddleware\Util\PemUtil;
// use GuzzleHttp\HandlerStack;
 

class WechatPay{
    
    
    //自定义支付金额输入
    public function CodePay(){
        return View::fetch();
    }
    
    /**
     * JsApiPay 下单接口
    **/
    public function JsApiPay(){
        
        	$tools = new \JsApiPay();
        	$config = new \WxPayConfig();

        	//获取id
        	$openId = $tools->GetOpenid();
            
            $pay_code = input('get.pay_code')?input('get.pay_code'):0000001;
		    $pay_title = input('get.pay_title')?input('get.pay_title'):"null";
        	$pay_time = input('get.pay_time')?input('get.pay_time'):time();
        	
        	//单号
        	$out_trade_no = "kyt_".date("YmdHis");
        	
        	
        	//$out_trade_no
        	//写入单号
        // 	$update = Db::connect('AIBM')->table('ai_kj')->where(['time'=>$pay_time,'code'=>$pay_code])->update(['out_trade_no'=>$out_trade_no]);
        	
        	$input = new \WxPayUnifiedOrder();
        	//商品描述
        	$input->SetBody($pay_title);
        	$input->SetOut_trade_no($out_trade_no);
        	$input->SetTotal_fee($pay_code*100);
        	$input->SetTime_start(date("YmdHis"));
        	$input->SetTime_expire(date("YmdHis", time() + 600));
        	$input->SetGoods_tag("test");
        	$input->SetNotify_url("https://www.kaiyuantong.cn/");
        	$input->SetTrade_type("JSAPI");
        	$input->SetOpenid($openId);
        	
        	$order = \WxPayApi::unifiedOrder($config, $input);
        	
        	//printf_info($order);
        	//获取jsapi支付的参数
        	$jsApiParameters = $tools->GetJsApiParameters($order);

        
        	//获取共享收货地址js函数参数
        	$editAddress = $tools->GetEditAddressParameters();
        	
        	View::assign('jsApiParameters',$jsApiParameters);//支付参数
        	View::assign('editAddress',$editAddress);//收货地址
        	View::assign('out_trade_no',$out_trade_no);//订单号
        	View::assign('title',isset($pay_title)?$pay_title:'');//订单标题
        	View::assign('code',isset($pay_code)?$pay_code:'');//支付金额
        	//dump($jsApiParameters);
        	return View::fetch();
    }
    
    
    public function pay_callback(){

        require_once APP_PATH . "common/wxpay/lib/WxPay.Api.php";
        $config = new \WxPayConfig();
        $msg = "";
        \WxPayApi::notify($config,function($result){
            //(array)$result;
		    //$result = json_decode(json_encode((array)$result),true);
		    //dump($result);
		  //  $out_trade_no = $result["*values"]['out_trade_no'];
		  $result = str_replace("\u0000*\u0000","",json_encode((array)$result));
		  $result = json_decode($result,true);
		  Db::connect('AIBM')->table('ai_kj')->where('out_trade_no',$result['values']['out_trade_no'])->save(['is_ok'=>1]);
		  //Db::connect('AIBM')->table('ai_kj')->where(['id'=>1])->save(['img'=>$result]);
		    
        },$msg);
        //  "code": "SUCCESS",
        //  "message": "成功"
    }
}