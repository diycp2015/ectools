<?php

// 自带函数
require(APP_PATH . 'ectools/common_en.php');// 英文函数
require(APP_PATH . 'ectools/common_zh.php');// 中文函数
require(APP_PATH . 'ectools/QueryData.php');// 查询数据库 中英合并
require(APP_PATH . 'ectools/Ras.php');// Ras 非对称加密

// 后台引入
require(APP_PATH . 'ectools/Admin/Base.php');
require(APP_PATH . 'ectools/Admin/EctAdmin.php');
require(APP_PATH . 'ectools/Admin/EctAdminApi.php');
require(APP_PATH . 'ectools/Admin/EctAdminJson.php');
require(APP_PATH . 'ectools/Admin/EctAdminLogin.php');
require(APP_PATH . 'ectools/Admin/ElectronApi.php');
require(APP_PATH . 'ectools/Admin/Vcode.php');
//require(APP_PATH . 'ectools_app/Admin/Vcode.php');

/**
* 生成密钥
*
**/
function getToken($lang = 12){
// 密码字符集，可任意添加你需要的字符
$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
$password = '';
for ( $i = 0; $i < $lang; $i++ )
{
// 这里提供两种字符获取方式
// 第一种是使用 substr 截取$chars中的任意一位字符；
// 第二种是取字符数组 $chars 的任意元素
//$password .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
$password .= $chars[mt_rand(0,strlen($chars) - 1)];
}
return $password;
}

// 检测核心库更新
function ect_check_updates(){
    if(file_get_contents(ect_api.'/ect_version.php') !== ect_version()){
        json_exit(0,'发现新版本');
    }
    
    json_exit(1,'版本一致 无需更新');
}

// 更新核心库
function ect_updates(){
    if(file_get_contents(ect_api.'/ect_version.php') !== ect_version()){
        $ok = copy(ect_api.'/ect.php', 'ect.php');
        if(!$ok){
            json_exit(1,'更新失败');
        }
        json_exit(0,'更新成功');
    }
    
    json_exit(1,'版本一致 无需更新');
}