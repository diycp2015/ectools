<?php
/**
 * 日志类库
 * 
 * 作者qq32579135
 * 官网：https://pmhapp.com
**/
use think\facade\Db;
use think\facade\Config;
use think\facade\Request;

class Log
{
    
    public static function runlog(){
        //日志构造器
        $data = $this->ConstructTheLog();
        $this->dblog($data['type'],$data['adminId'],$data['url'],$data['place'],$data['remark']);
    }
    /**
     * dblog 写入日志到数据库
     * 
     * 作者qq32579135
     * 官网：https://pmhapp.com
     * 
     * $type @类别 用于区分日志类别 1=>后台操作日志 2=>前端接口日志
     * $adminId @操作用户身份id
     * $url @操作接口
     * $place @用户位置
     * $remark @备注信息
    **/
    protected function dblog($type = 1,$adminId = 1,$url = 'null',$place = 'null',$remark = 'null'){
        $ok = Config::get('app.ect_log');
        if($ok){
            switch ($type)
            {
            case 1:
                //对应EctAdminApi.php
                $data['name'] = '后台操作日志';
                $data['type'] = $type;
                $data['adminId'] = $adminId;
                $data['url'] = $url;
                $data['place'] = $place;
                $data['remark'] = $remark;
                $data['time'] = time();
                
                $insert = Db::name('log')->insert($data);
    
                break;
            case 2:
                $data['name'] = '前端接口日志';
                $data['type'] = $type;
                $data['adminId'] = $adminId;
                $data['url'] = $url;
                $data['place'] = $place;
                $data['remark'] = $remark;
                $data['time'] = time();
                
                $insert = Db::name('log')->insert($data);
                break;
            default:
            }
        }
        
    }

    /**
     * json_exit @返回json数据
     * 
     * $code @状态码
     * $msg @返回结果
    **/
    
    protected function json_exit($code = 0,$msg = 'ok',$value = ''){
        exit(json_encode(['code'=>$code,'msg'=>$msg,'value'=>$value]));
    }
    
    
        
    /**
     * 日志构造器
     * 
     * 作者qq32579135
     * 官网：https://pmhapp.com
     * 
     * $type @类别 用于区分日志类别 1=>后台操作日志 2=>前端接口日志
     * $adminId @操作用户身份id
     * $url @操作接口
     * $place @用户位置
     * $remark @备注信息
    **/    
    protected function ConstructTheLog(){
        
        $data['type'] = 1;
        $data['adminId'] = 1;
        $data['url'] = Request::url();
        $data['place'] = $_SERVER["REMOTE_ADDR"];
        $data['remark'] = Request::method(true);
        
        return $data;
    }
}