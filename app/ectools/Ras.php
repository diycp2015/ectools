<?php

/**
 * 使用生成RSA密钥
 * 
 * $publicKey @ 公密
 * $privateKey @ 私密
**/
function genKeys(){
    $resource = openssl_pkey_new();
    $privateKey ='';
    
    openssl_pkey_export($resource, $privateKey);

    $detail = openssl_pkey_get_details($resource);
    $publicKey = $detail['key'];
    
    $data = ['publicKey'=>$publicKey,'privateKey'=>$privateKey];
    return $data;
}

/**
 * 使用私钥进行加密
 * 
 * $data @ 要加密的数组
 * $privateKey @ 私钥
**/
function privateEncode(array $data,String $privateKey) {
    $data = json_encode($data);
    
    $result = openssl_private_encrypt($data, $encrypted,  $privateKey);
    
    if($result === false) {
        return NULL;
    } else {
        return base64_encode($encrypted);
    }
}

/**
 * 使用公密进行解密
 * 
 * $data @ 私钥加密后的内容 base64
 * $publicKey @ 公钥
**/
function publicDecode($data,String $publicKey) {
    //$data = json_encode($data);
    
    $result = openssl_public_decrypt(base64_decode($data), $decrypted,$publicKey);
    if($result === false) {
        return NULL;
    } else {
        return json_decode($decrypted, true);
    }

}

// $data = genKeys();
// //var_dump($data);
// $a = privateEncode(['id'=>12321],$data['privateKey']);
// var_dump($a);
// var_dump(publicDecode($a,$data['publicKey']));