<?php
// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | QQ：32579135
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use think\facade\Config;
use think\facade\View;//视图类库
use think\facade\Request;


//中文转移方法


/**
 * 断是否是微信内置浏览器
 * 
 * 作者qq32579135
 * 官网：https://pmhapp.com
 * 
**/
function 判断微信() { 
  if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) { 
    return true; 
  } else {
    return false; 
  }
}


//视图层渲染变量
function 变量渲染($key,$value){
    View::assign($key,$value);
}

/**
 * json_exit @返回json数据
 * 
 * 作者qq32579135
 * 官网：https://pmhapp.com
 * 
 * $code @状态码
 * $msg @返回结果
**/
function 返回_json($code = 0,$msg = 'ok',$value = ''){
    exit(json_encode(['code'=>$code,'msg'=>$msg,'value'=>$value],JSON_UNESCAPED_UNICODE));
}


function 视图渲染($file = ''){
    $str = Request::pathinfo();
    // dump($str);
    if($str){
        $data = explode("/",$str);
        if($file == ''){
            return View::fetch($data[0] . '/' . $data[2]);
        }else{
            return View::fetch($data[0] . '/' . $file);
        }
    }else{
        return View::fetch(Request::controller());
    }
}

function 打印($data){
    dump($data);
}

