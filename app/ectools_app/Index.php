<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | 微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use app\BaseController;
use think\facade\Cache;
use baidufanyi\Fanyi;
use think\facade\Cookie;


class Index extends BaseController
{
    public function index(){

        // 默认界面
        return '<div style="text-align:center;">' . title() . '<br> ｜高质量｜低负荷｜ <br/>最主流的快速开发框架，助力前端开发者 服务端小白也可轻松打造Api接口 web端程序 <br ></div>';

    }
}