-- MySQL dump 10.13  Distrib 5.6.50, for Linux (x86_64)
--
-- Host: localhost    Database: e_kaiyuantong_cn
-- ------------------------------------------------------
-- Server version	5.6.50-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ect_admin`
--

DROP TABLE IF EXISTS `ect_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ect_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) NOT NULL COMMENT '登录名称',
  `password` varchar(255) NOT NULL COMMENT '登录密码',
  `type` int(11) NOT NULL COMMENT '身份',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ect_admin`
--

LOCK TABLES `ect_admin` WRITE;
/*!40000 ALTER TABLE `ect_admin` DISABLE KEYS */;
INSERT INTO `ect_admin` VALUES (1,'admin','c3284d0f94606de1fd2af172aba15bf3',0);
/*!40000 ALTER TABLE `ect_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ect_config`
--

DROP TABLE IF EXISTS `ect_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ect_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(5000) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ect_config`
--

LOCK TABLES `ect_config` WRITE;
/*!40000 ALTER TABLE `ect_config` DISABLE KEYS */;
INSERT INTO `ect_config` VALUES ('ectToken',''),('show_error_msg','true\r\n'),('web_config','{\"web_title\":\"\",\"web_logo\":\"\\/storage\\/logo\\/20220215\\/1730607186b89bae6e238f8572660bbb.png\",\"web_seo_key\":\"\",\"web_seo_desc\":\"\",\"web_copyright\":\"\",\"web_icp\":\"\"}');
/*!40000 ALTER TABLE `ect_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ect_log`
--

DROP TABLE IF EXISTS `ect_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ect_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) NOT NULL COMMENT '操作名称',
  `type` int(10) NOT NULL COMMENT '类型id',
  `adminId` int(10) NOT NULL COMMENT '管理员id',
  `url` varchar(255) NOT NULL COMMENT '请求接口',
  `place` varchar(255) NOT NULL COMMENT '用户位置',
  `remark` varchar(255) NOT NULL COMMENT '备注信息',
  `time` varchar(255) NOT NULL COMMENT '操作时间',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14596 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ect_log`
--

--
-- Table structure for table `ect_menu`
--

DROP TABLE IF EXISTS `ect_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ect_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `gid` int(10) NOT NULL COMMENT '父级id',
  `title` varchar(255) NOT NULL COMMENT '名称',
  `icon` varchar(255) NOT NULL COMMENT '菜单图标',
  `href` varchar(255) NOT NULL COMMENT '菜单地址',
  `target` varchar(64) NOT NULL DEFAULT '_self' COMMENT '打开方式',
  `sort_id` int(10) NOT NULL COMMENT '菜单排序',
  `status` int(10) NOT NULL COMMENT '显示状态 状态(0:禁用,1:启用)',
  `remark` varchar(255) NOT NULL COMMENT '菜单备注',
  `create_at` varchar(255) NOT NULL COMMENT '创建时间',
  `update_at` varchar(255) NOT NULL COMMENT '修改时间',
  `delete_at` varchar(255) NOT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ect_menu`
--

LOCK TABLES `ect_menu` WRITE;
/*!40000 ALTER TABLE `ect_menu` DISABLE KEYS */;
INSERT INTO `ect_menu` VALUES (1,0,'网站管理','','','_self',1,1,'','','',''),(2,1,'网站设置','fa fa-cog','','_self',1,1,'','','',''),(3,2,'系统配置','','/set_system','_self',2,1,'','','',''),(4,2,'网站配置','','/set_web\r\n','_self',8,1,'','','',''),(5,1,'网站日志','fa fa-calendar','/ect_log','_self',1,1,'','','',''),(6,1,'系统信息','fa fa-hdd-o','/ect_system_info','_self',1,1,'','','',''),(7,0,'程序开发','','','_self',1,1,'','','',''),(8,7,'Ect 在线版','fa fa-code','/program','_self',1,1,'','','',''),(9,0,'全站配置','','','_self',1,1,'','','',''),(10,7,'Ect 桌面端','fa fa-sitemap','/EctCode','_self',1,1,'','','',''),(11,7,'Ect 插件库','fa fa-puzzle-piece','/EctPlugin','_self',1,1,'','','',''),(12,7,'Ect 应用库','fa fa-cubes','/EctApp','_self',1,1,'','','','');
/*!40000 ALTER TABLE `ect_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ect_panel`
--

DROP TABLE IF EXISTS `ect_panel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ect_panel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ect_panel`
--

LOCK TABLES `ect_panel` WRITE;
/*!40000 ALTER TABLE `ect_panel` DISABLE KEYS */;
INSERT INTO `ect_panel` VALUES (1,'e_kaiyuantong_cn','ttRekLSNz5FKjZXN','27ca343d18cd53c6a3841de5c28909fd',1);
/*!40000 ALTER TABLE `ect_panel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'e_kaiyuantong_cn'
--

--
-- Dumping routines for database 'e_kaiyuantong_cn'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-01 16:48:16
