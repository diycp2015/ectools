<?php
// 应用公共文件
use think\facade\Config;
use think\facade\View;//视图类库
use think\facade\Request;
use think\facade\Cache;
use think\facade\Cookie;
// ect核心库版本号
function ect_version(){
return "2.0.7";
}
/**
* 获取网站所在一级目录
**/
function RootPath(){
return app()->getRootPath();
}
/**
* 修改文件名
*
* $file @原名字
* $new_file @新名字
*
* $filee @所在目录
**/
function set_rename($file,$new_file,$filee){
if(file_exists($filee . $file)){
//chdir($filee);
if(rename($filee.$file,$filee.$new_file)){
return $file.' 重命名成功！';
}else{
return $file.' 重命名失败！';
}
}else{
return $file.' 不存在！';
}
}
/**
* set_chmod @修改文件内容 确保文件所属www
*
* 作者wx: N79823
* 官网：https://pmhapp.com
*
* $file @路径
* $text @修改前
* $_text @修改后
**/
function set_chmod($file,$text,$_text){
chmod($file,0777);
$file_get_contents = file_get_contents($file);//读取内容
$_file_get_contents = str_replace($text,$_text,$file_get_contents);//修改内容
file_put_contents($file,$_file_get_contents);//写入
chmod($file,0644);
}
/**
* system_info @获取服务器相关信息
*
* 作者wx: N79823
* 官网：https://pmhapp.com
*
**/
function system_info(){
//php版本信息
$data['PHP_VERSION'] = PHP_VERSION;
//mysql版本信息
$hostname = Config::get('database.connections.mysql.hostname');
$username = Config::get('database.connections.mysql.username');
$password = Config::get('database.connections.mysql.password');
$mysql = mysqli_connect($hostname,$username,$password);
$data['MYSQL_VERSION'] = mysqli_get_server_info($mysql);
//访问者客户端信息
$data['HTTP_USER_AGENT'] = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'null';
//cpu数量
$data['PROCESSOR_IDENTIFIER'] = isset($_SERVER['PROCESSOR_IDENTIFIER'])?$_SERVER['PROCESSOR_IDENTIFIER']:'1';
//服务器解析器
$data['SERVER_SOFTWARE'] = isset($_SERVER['SERVER_SOFTWARE'])?$_SERVER['SERVER_SOFTWARE']:'null';
//获取服务器语言
$data['HTTP_ACCEPT_LANGUAGE'] = isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])?$_SERVER['HTTP_ACCEPT_LANGUAGE']:'null';
//获取服务器Web端口
$data['SERVER_PORT'] = isset($_SERVER['SERVER_PORT'])?$_SERVER['SERVER_PORT']:'null';
//获取系统类型及版本号
$data['php_uname'] = php_uname();
//phpxiang guang
//已加载扩展库
$data['php_loaded_extensions'] = get_loaded_extensions();
//禁用函数
$data['disable_functions'] = strsToArray((ini_get('disable_functions')));
//PHP安全模式(Safe_mode
if (ini_get("safe_mode")==0){
$data['safe_mode'] = 'OFF';
}else{
$data['safe_mode'] = 'ON';
}
//PHP 运行方式
$data['php_sapi_name'] = php_sapi_name();
//php 上传大小限制
$data['upload_max_filesize'] = min(intval(get_cfg_var('upload_max_filesize')),intval(get_cfg_var('post_max_size')),intval(get_cfg_var('memory_limit'))) . 'Mb';
//ini_get_all()高级
//服务器脚本超时时间 ini_get("max_execution_time")
$data['max_execution_time'] = ini_get('max_execution_time').'秒';
//POST提交大小限制
$data['post_max_size'] = ini_get('post_max_size');
return $data;
}
/**
* 字符串转数组
*
* 作者wx: N79823
* 官网：https://pmhapp.com
*
**/
function strsToArray($strs,$fuhao = ",") {
$result = array();
$array = array();
$strs = str_replace('，', $fuhao, $strs);
// $strs = str_replace("n", ',', $strs);
// $strs = str_replace("rn", ',', $strs);
$strs = str_replace(' ', $fuhao, $strs);
$array = explode($fuhao, $strs);
foreach ($array as $key => $value) {
if ('' != ($value = trim($value))) {
$result[] = $value;
}
}
return $result;
}
/**
* 获取文件名去除后缀
*
* 作者wx: N79823
* 官网：https://pmhapp.com
*
**/
function file_0($filename){
$houzhui = substr(strrchr($filename, '.'), 1);
$result = basename($filename,".".$houzhui);
return $result;
}
/**
* 获取文件名后缀
*
* 作者wx: N79823
* 官网：https://pmhapp.com
*
**/
function file_1($file_name){
$retval = "";
$pt = strrpos ( $file_name , "." );
if ( $pt ) $retval = substr ( $file_name , $pt +1, strlen ( $file_name ) - $pt );
return ( $retval );
}
/**
* 获取文件下目录
*
* 作者wx: N79823
* 官网：https://pmhapp.com
*
**/
function getDirContent($path){
if(!is_dir($path)){
return false;
}
//readdir方法
/* $dir = opendir($path);
$arr = array();
while($content = readdir($dir)){
if($content != '.' && $content != '..'){
$arr[] = $content;
}
}
closedir($dir); */
//scandir方法
$arr = array();
$data = scandir($path);
foreach ($data as $value){
if($value != '.' && $value != '..'){
$arr[] = $value;
}
}
return $arr;
}
/**
* 获取当前目录下的文件夹
*
* @param string $path 操作目录（注意：路径结尾不要有斜杠/）
* return array 目录下的所有文件夹
*/
function getFolderList($path)
{
$folderList = []; //最终返回的数组
//扫描目录内的所有目录和文件并返回数组
$data = scandir($path);
foreach ($data as $value) {
//判断如果不是文件夹则进入下一次循环
if (!is_dir($path . "/" . $value)) {
continue;
}
if ($value != '.' && $value != '..') {
$folderList[] = $value;
}
}
return $folderList;
}
/**
* php除数组指定的key值(直接删除key值实现)
* @param unknown $data
* @param unknown $key
* @return unknown
*/
function array_remove($data, $key){
if(!array_key_exists($key, $data)){
return $data;
}
$keys = array_keys($data);
$index = array_search($key, $keys);
if($index !== FALSE){
array_splice($data, $index, 1);
}
return $data;
}
//xss安全过滤
function remove_xss($val) {
// remove all non-printable characters. CR(0a) and LF(0b) and TAB(9) are allowed
// this prevents some character re-spacing such as <java\0script>
// note that you have to handle splits with \n, \r, and \t later since they *are* allowed in some inputs
$val = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $val);
// straight replacements, the user should never need these since they're normal characters
// this prevents like <IMG SRC=@avascript:alert('XSS')>
$search = 'abcdefghijklmnopqrstuvwxyz';
$search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$search .= '1234567890!@#$%^&*()';
$search .= '~`";:?+/={}[]-_|\'\\';
for ($i = 0; $i < strlen($search); $i++) {
// ;? matches the ;, which is optional
// 0{0,7} matches any padded zeros, which are optional and go up to 8 chars
// @ @ search for the hex values
$val = preg_replace('/(&#[xX]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val); // with a ;
// @ @ 0{0,7} matches '0' zero to seven times
$val = preg_replace('/(�{0,8}'.ord($search[$i]).';?)/', $search[$i], $val); // with a ;
}
// now the only remaining whitespace attacks are \t, \n, and \r
$ra1 = array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
$ra2 = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
$ra = array_merge($ra1, $ra2);
$found = true; // keep replacing as long as the previous round replaced something
while ($found == true) {
$val_before = $val;
for ($i = 0; $i < sizeof($ra); $i++) {
$pattern = '/';
for ($j = 0; $j < strlen($ra[$i]); $j++) {
if ($j > 0) {
$pattern .= '(';
$pattern .= '(&#[xX]0{0,8}([9ab]);)';
$pattern .= '|';
$pattern .= '|(�{0,8}([9|10|13]);)';
$pattern .= ')*';
}
$pattern .= $ra[$i][$j];
}
$pattern .= '/i';
$replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2); // add in <> to nerf the tag
$val = preg_replace($pattern, $replacement, $val); // filter out the hex tags
if ($val_before == $val) {
// no replacements were made, so exit the loop
$found = false;
}
}
}
return $val;
}
// get请求方法封装 来源：https://www.runoob.com
function geturl($url){
$headerArray = array("Content-type:application/json;","Accept:application/json");
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch,CURLOPT_HTTPHEADER,$headerArray);
$output = curl_exec($ch);
curl_close($ch);
$output = json_decode($output,true);
return $output;
}
function posturl($url,$data){
$data = json_encode($data);
$headerArray =array("Content-type:application/json;charset='utf-8'","Accept:application/json");
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
curl_setopt($curl,CURLOPT_HTTPHEADER,$headerArray);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($curl);
curl_close($curl);
return json_decode($output,true);
}
function YunTu($url)
{
header("Content-Type:text/json;charset=UTF-8");
$ch = curl_init();
$timeout = 30;
$ua= $_SERVER['HTTP_USER_AGENT'];
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
$http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
$urls = $http_type . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_REFERER,$urls);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.$ip, 'CLIENT-IP:'.$ip));
curl_setopt($ch, CURLOPT_USERAGENT, $ua);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
$content = curl_exec($ch);
curl_close($ch);
return $content;
}
//删除文件夹
function deldir($dir) {
//先删除目录下的文件：
$dh=opendir($dir);
while ($file=readdir($dh)) {
if($file!="." && $file!="..") {
$fullpath=$dir."/".$file;
if(!is_dir($fullpath)) {
unlink($fullpath);
} else {
deldir($fullpath);
}
}
}
closedir($dh);
//删除当前文件夹：
if(rmdir($dir)) {
return true;
} else {
return false;
}
}
/**zip导出 https://www.php.net/manual/zh/book.zip.php
* bt宝塔面板php7.3、php7.4不支持ZipArchive解决方法
* https://www.jb51.net/article/188913.htm
*
* php7.4 ssh终端执行以下
* cd /www/server/php/74/src/ext/zip/
/www/server/php/74/bin/phpize
./configure --with-php-config=/www/server/php/74/bin/php-config
make && make install
echo "extension = zip.so" >> /www/server/php/74/etc/php.ini
*
* 参考：https://blog.csdn.net/hzthis/article/details/107488086
**/
function addFileToZip($path,$zip,$getname='',$name='')
{
$handler = opendir($path);
while (($filename = readdir($handler)) !== false) {
if ($filename != "." && $filename != "..") {
if (is_dir($path . "/" . $filename)) {
addFileToZip($path . "/" . $filename, $zip);
} else {
$zip->addFile($path . "/" . $filename,$getname . '/' . $name . '/' . $getname . '/' .$filename);
}
}
}
}
//批量导出zip
function dirToZip($paths,$filename,$getname = '')
{
$zip = new \ZipArchive();
try {
if ($zip->open($filename, $zip::CREATE) == TRUE) {
foreach ($paths as $path){
if($paths[0] == $path){$name = 'ectools_app';}else{$name = 'ectools_view';}
addFileToZip($path, $zip, $getname, $name);
}
$zip->close();
}
if(file_exists($filename)){
return ['code'=>200,'url'=>$filename,'msg'=>'success'];
}
return ['code'=>-200,'msg'=>'打包失败'];
} catch (\Exception $e) {
return ['code'=>-200,'msg'=>$e->getMessage()];
}
}
//还原默认入口文件名
function reduction_default_controller(){
$default_controller = Config::get('route.default_controller');
$file = config_path() . 'route.php';
set_chmod($file,'$default_controller = "' . $default_controller . '";','$default_controller = "index";');
}
function title(){
return Config::get('app.html');
}
// 取出app
function EctApp(){
$_file = ECT_APP_PATH;
$app_list = getFolderList($_file);
$b = [];//临时变量
$c = [];//临时变量
$app = [];//app程序
foreach ($app_list as $value){
$file = ECT_APP_PATH . $value;
$b[$value] = getDirContent($file);
}
//读取后缀
foreach ($b as $key => $val){
if(!empty($b[$key])){
$d = [];
foreach ($b[$key] as $val){
$name = file_1($val);
$d[] = $name;
}
$b[$key] = $d;
}
}
//重构app
$app_fonfig = [];
$k = 0;
foreach ($b as $key => $val){
if(!empty($b[$key]) && in_array("json",$val)){
in_array("json",$val);
$json_string = file_get_contents($_file . '/' . $key.'/config.json');
if(!is_json($json_string)){
$data = json_decode($json_string, true);
$data['file_name'] = $key;
$app[] = $key;
$app_fonfig[$k] = $data;
$k++;
}
}
}
//dump($app_fonfig);
return $app_fonfig;
}
// 生成随机token $length 长度
function GetRandStr($length){
//字符组合
$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
$len = strlen($str)-1;
$randstr = '';
for ($i=0;$i<$length;$i++) {
$num=mt_rand(0,$len);
$randstr .= $str[$num];
}
return $randstr;
}
/*
* 检测服务器是否支持SSL连接(Https连接)
* @return bool
*/
function is_SSL(){
if(!isset($_SERVER['HTTPS']))
return false;
if($_SERVER['HTTPS']===1){ //Apache
return true;
}elseif($_SERVER['HTTPS']==='on'){ //IIS
return true;
}elseif($_SERVER['SERVER_PORT']==443){ //其他
return true;
}
return false;
}
/**
文件上传接口
$fileRoute 存储文件名 必须 统一存储到public/storage 目录下
$fileSize 文件大小MB
$fileExt 文件格式
**/
function upload_file($fileRoute,$fileSize = 5,$fileExt = 'jpg,png,jpeg'){
// 获取表单上传文件-
// $files = request()->file();
// //exit(json_encode(['code'=>0,'msg'=>$file]));
// try {
// validate(['file'=>['fileSize'=>$fileSize*1024*1024,'fileExt'=>$fileExt]])
// ->check(['file'=>$files]);
// foreach ($files as $file){
// $savename = \think\facade\Filesystem::disk('public')->putFile( $fileRoute, $file);
// exit(json_encode(['code'=>0,'msg'=>'上传成功','imgUrl'=>'/storage/'.$savename]));
// }
// } catch (\think\exception\ValidateException $e) {
// exit(json_encode(['code'=>1,'msg'=>$e->getMessage()]));
// }
header('Content-type:text/html;charset=utf-8');
if(!empty($_FILES['file']['name'])){
if($_FILES['file']['error'] == 0){ // 判断上传是否正确
$fileName = $_FILES['file']['name']; // 获取文件名称
$fileSize = $_FILES['file']['size']; // 获取文件大小
$tmp_name = $_FILES["file"]["tmp_name"]; // 获取上传文件默认临时地址
$fileTypeInfo = strsToArray($fileExt,","); // 定义允许上传文件类型【很多种只列举3种】
$fileType = substr(strrchr($fileName,'.'),1); // 提取文件后缀名
if(!in_array($fileType,$fileTypeInfo)){ // 判断该文件是否为允许上传的类型
exit(json_encode(['code'=>1,'msg'=>'格式不正确']));// 显示错误信息
}
if($fileSize/1024 > 1024*$fileSize){ // 规定文件上传大小【文件为Byte/1024 转为 kb】
exit(json_encode(['code'=>1,'msg'=>'文件太大']));// 显示错误信息
}
$path = public_path() . '/storage/'. $fileRoute .'/';
if(!file_exists($path)){ // 判断是否存在存放上传文件的目录
mkdir($path); // 建立新的目录
}
move_uploaded_file($tmp_name, $path . $newFileName); // 移动文件到指定目录
exit(json_encode(['code'=>0,'msg'=>'上传成功','value'=> '/storage/'. $fileRoute . '/' . $fileName]));
}else{
exit(json_encode(['code'=>1,'msg'=>'上传失败']));
}
}
}
/**
* @Author: JMJ 458902862@qq.com
* @Date: 2022-09-25 01:43:00
* @LastEditors: JMJ
* @Description: layui返回格式封装
*/
function laytable($msg,$count,$data){
$info = [
'code' => 0,
'msg' => $msg,
'count' => $count,
'data' => $data
];
return $info;
}
/**
* @Author: JMJ 458902862@qq.com
* @Date: 2022-10-1 00:43:00
* @LastEditors: JMJ
* @Description: 返回格式封装
*/
function ret_info($code,$msg='',$data=[]){
$info = [
'code' => $code,
'msg' => $msg,
'data' => $data
];
return json($info);
}
/**
* 经纬度计算距离
*
* 返回结果米
* $place1 @经纬度 数组格式
* $place2 @经纬度 数组格式
**/
function getDistance($place1, $place2){
$lng1 = $place1[0];
$lat1 = $place1[1];
$lng2 = $place2[0];
$lat2 = $place2[1];
$earthRadius = 6367000; //approximate radius of earth in meters
$lat1 = ($lat1 * pi() ) / 180;
$lng1 = ($lng1 * pi() ) / 180;
$lat2 = ($lat2 * pi() ) / 180;
$lng2 = ($lng2 * pi() ) / 180;
$calcLongitude = $lng2 - $lng1;
$calcLatitude = $lat2 - $lat1;
$stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
$stepTwo = 2 * asin(min(1, sqrt($stepOne)));
$calculatedDistance = $earthRadius * $stepTwo;
return round($calculatedDistance) - 470;
// $lat1 = $place1[0];
// $lat2 = $place1[1];
// $lng1 = $place2[0];
// $lng2 = $place2[1];
// //将角度转为狐度
// $radLat1=deg2rad($lat1);
// $radLat2=deg2rad($lat2);
// $radLng1=deg2rad($lng1);
// $radLng2=deg2rad($lng2);
// $a=$radLat1-$radLat2;//两纬度之差,纬度<90
// $b=$radLng1-$radLng2;//两经度之差纬度<180
// $s=2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137;
// return $s;
}
/**
* 二维数组根据某个字段排序
* @param array $array 要排序的数组
* @param string $keys 要排序的键字段
* @param string $sort 排序类型 SORT_ASC SORT_DESC
* @return array 排序后的数组
*/
function arraySort($array, $keys, $sort = SORT_DESC) {
$keysValue = [];
foreach ($array as $k => $v) {
$keysValue[$k] = $v[$keys];
}
array_multisort($keysValue, $sort, $array);
return $array;
}
// 判断是否是json格式
/**
* 判断是否合法json
*/
function is_json($string) {
json_decode($string);
if(json_last_error() == JSON_ERROR_NONE){
return false;
}else{
return true;
}
}
/**
* 翻译文字
**/
// function langfanyi($string){
// $lang = Cookie::get('think_lang');
// if(!$lang || $lang == null){
// echo $string;
// }else{
// echo (new \baidufanyi\Fanyi)::Universal($string,$lang);
// }
// }
/**
* file_get_contents post 请求
**/
function fgc_post_ask($url,Array $data){
$postdata = http_build_query($data);
$opts = array('http' =>
array(
'method' => 'POST',
'header' => 'Content-Type: application/x-www-form-urlencoded',
'content' => $postdata
)
);
$context = stream_context_create($opts);
return file_get_contents($url, false, $context);
}


/** 文件转base64输出

* @param  String $file 文件路径

* @return String base64 string

*/

function fileToBase64($file){

    $base64_file = '';

    if(file_exists($file)){
        dump(file_get_contents($file));
        $base64_data = base64_encode(file_get_contents($file));

        $base64_file = $base64_data;

    }

    return $base64_file;

}

  

/** base64转文件输出

* @param  String $base64_data base64数据

* @param  String $file        要保存的文件路径

* @return boolean

*/

function base64ToFile($base64_data, $file){

    if(!$base64_data || !$file){

        return false;

    }

    return file_put_contents($file, base64_decode($base64_data), true);

}
// +----------------------------------------------------------------------
// | 杂项相关 END
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | 微信公众号相关
// +----------------------------------------------------------------------
/**
* 获取Access token 7200s有效
*
* $APPID @ appid
* $APPSECRET @ AppSecret
**/
function wx_get_at($APPID,$APPSECRET){
$access_token = Cache::get('access_token');
// dump($access_token);
// //return $access_token;
// exit();
if(!$access_token){
$geturl = geturl("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $APPID. "&secret=". $APPSECRET);
$access_token = !empty($geturl['access_token'])?$geturl['access_token']:'';
if($access_token){
Cache::set('access_token', $access_token, 7100);
}
return $access_token;
}else{
return $access_token;
}
}
/**
* 发送模板消息
* $ACCESS_TOKEN @ ACCESS_TOKEN
* https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=
**/
function wx_post_messaget($ACCESS_TOKEN,$data){
$headerArray =array("Content-type:application/json;charset='utf-8'","Accept:application/json");
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $ACCESS_TOKEN);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
curl_setopt($curl,CURLOPT_HTTPHEADER,$headerArray);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($curl);
curl_close($curl);
}
// +----------------------------------------------------------------------
// | 微信公众号相关 END
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | 微信小程序相关
// +----------------------------------------------------------------------
//图片合法性验证
function http_request($url, $data = null)
{
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
if (!empty($data)) {
curl_setopt($curl, CURLOPT_POST, TRUE);
curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json'
));
}
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
$output = curl_exec($curl);
curl_close($curl);
return $output;
exit();
}
// 小程序获取手机号
// $ACCESS_TOKEN @
// $code 小程序端获取的code参数
function wx_post_PhoneNumber($ACCESS_TOKEN,$code){
$data['code'] = $code;
$url = "https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=$ACCESS_TOKEN";
$info = http_request($url,json_encode($data),'json');
// 一定要注意转json，否则汇报47001错误
// exit(dump($info));
return $info;
//返回格式{"errcode":0,"errmsg":"ok","phone_info":{"phoneNumber":"shoujihao","purePhoneNumber":"17637944405","countryCode":"86","watermark":{"时间戳":1662875212,"appid":"wxefd1f48669733f8c"}}}
// exit(dump($tmpinfo));
// if($tmpinfo['errcode'] > 0){
// json_exit(1,$tmpinfo['errmsg']);
// }
// $code = $tmpinfo['code'];
// $phoneNumber = $tmpinfo['phone_info']['phoneNumber'];
// json_exit(1,$tmpinfo['errmsg'],$phoneNumber);
}
/**
* 微信小程序登录
*
* $appid @ 小程序 appId
* $appSecret @ 小程序 appSecret
* $js_code @ 登录时获取的 code，可通过wx.login获取
*
{
"openid":"xxxxxx",
"session_key":"xxxxx",
"unionid":"xxxxx",
"errcode":0,
"errmsg":"xxxxx"
}
https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/login.html
*
**/
function wx_get_code2Session($appid,$appSecret,$js_code,$grant_type = 'authorization_code'){
$url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' . $appid . '&secret=' . $appSecret . '&js_code=' . $js_code . '&grant_type' . $grant_type;
$data = geturl($url);
return $data;
}
/**
* 行插入数据
*
* @$file 文件路径
* @$content 内容
* @$Line 第几行
* @$index 第几个字符串开始
**/
function insert_data($file,$content,$Line,$index = 0){
$arrInsert = insertContent($file, $content . "\n" , $Line ,$index);
unlink($file);
foreach($arrInsert as $value){
file_put_contents($file, $value, FILE_APPEND);
}
}
function insertContent($source, $s, $iLine, $index) {
$file_handle = fopen($source, "r");
$i = 0;
$arr = array();
while (!feof($file_handle)) {
$line = fgets($file_handle);
++$i;
if ($i == $iLine) {
if($index == strlen($line)-1)
$arr[] = substr($line, 0, strlen($line)-1) . $s . "n";
else
$arr[] = substr($line, 0, $index) . $s . substr($line, $index);
}else {
$arr[] = $line;
}
}
fclose($file_handle);
return $arr;
}
// +----------------------------------------------------------------------
// | 微信小程序相关end
// +----------------------------------------------------------------------
