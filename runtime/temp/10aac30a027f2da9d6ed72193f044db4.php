<?php /*a:3:{s:68:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/duindex.htm";i:1650790051;s:73:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/extends/Base.htm";i:1644397262;s:74:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/public/jquery.htm";i:1651666477;}*/ ?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/css/public.css" media="all">
    <style>
    input{color:#76838f;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        
 <style>
        .layui-card {border:1px solid #f2f2f2;border-radius:5px;}
        .icon {margin-right:10px;color:#1aa094;}
        .icon-cray {color:#ffb800!important;}
        .icon-blue {color:#1e9fff!important;}
        .icon-tip {color:#ff5722!important;}
        .layuimini-qiuck-module {text-align:center;margin-top: 10px}
        .layuimini-qiuck-module a i {display:inline-block;width:100%;height:60px;line-height:60px;text-align:center;border-radius:2px;font-size:30px;background-color:#F8F8F8;color:#333;transition:all .3s;-webkit-transition:all .3s;}
        .layuimini-qiuck-module a cite {position:relative;top:2px;display:block;color:#666;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;font-size:14px;}
        .welcome-module {width:100%;height:210px;}
        .panel {background-color:#fff;border:1px solid transparent;border-radius:3px;-webkit-box-shadow:0 1px 1px rgba(0,0,0,.05);box-shadow:0 1px 1px rgba(0,0,0,.05)}
        .panel-body {padding:10px}
        .panel-title {margin-top:0;margin-bottom:0;font-size:12px;color:inherit}
        .label {display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em;margin-top: .3em;}
        .layui-red {color:red}
        .main_btn > p {height:40px;}
        .layui-bg-number {background-color:#F8F8F8;}
        .layuimini-notice:hover {background:#f6f6f6;}
        .layuimini-notice {padding:7px 16px;clear:both;font-size:12px !important;cursor:pointer;position:relative;transition:background 0.2s ease-in-out;}
        .layuimini-notice-title,.layuimini-notice-label {
            padding-right: 70px !important;text-overflow:ellipsis!important;overflow:hidden!important;white-space:nowrap!important;}
        .layuimini-notice-title {line-height:28px;font-size:14px;}
        .layuimini-notice-extra {position:absolute;top:50%;margin-top:-8px;right:16px;display:inline-block;height:16px;color:#999;}
</style>
<link rel="stylesheet" href="/static/admin/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md6">
                        <div class="layui-card">
                            <div class="layui-card-header"><i class="fa fa-plug icon icon-blue"></i>配置数据</div>
                            <div class="layui-card-body">
                                <div class="welcome-module">
                                    <div class="layui-row layui-col-space10">
                                        
                                        <div class="layui-tab" lay-filter="test">
                                          <ul class="layui-tab-title">
                                            <li class="layui-this" lay-id="11">网站设置</li>
                                            <li lay-id="22">系统配置</li>
                                          </ul>
                                          <div class="layui-tab-content">
                                            <div class="layui-tab-item layui-show">
                                              <!--系统配置-->
                                              <?php echo !empty($data) ? '已配置项目：<br/>' : ''; if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                              <?php echo htmlentities($vo); ?><br/>
                                              <?php endforeach; endif; else: echo "$empty" ;endif; ?>
                                            </div>
                                            <div class="layui-tab-item">
                                              <!--系统配置-->
                                            </div>
                                          </div>
                                        </div>
                                                                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md6">
                        <div class="layui-card">
                            <div class="layui-card-header"><i class="fa fa-credit-card icon icon-blue"></i>快捷入口</div>
                            <div class="layui-card-body">
                                <div class="welcome-module">
                                    <div class="layui-row layui-col-space10 layuimini-qiuck">
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" layuimini-content-href="/ect_log" data-title="菜单管理" data-icon="fa fa-window-maximize">
                                                <i class="fa fa-window-maximize"></i>
                                                <cite>网站日志</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" layuimini-content-href="/ect_system_info" data-title="系统设置" data-icon="fa fa-gears">
                                                <i class="fa fa-gears"></i>
                                                <cite>系统信息</cite>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header"><i class="fa fa-line-chart icon"></i>报表统计</div>
                            <div class="layui-card-body">
                                <div id="echarts-records" style="width: 100%;min-height:500px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>


    </div>
</div>
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/static/admin/js/message.js"></script>
<script>
    function ectmsg(title,code){

            switch(code)
            {
                case 0:
                    $.message({
                        message:title,
                        type:'success'
                    });
                    break;
                case 1:
                    $.message({
                        message:title,
                        type:'error'
                    });
                    break;
                case 2:
                    $.message({
                        message:title,
                        type:'warning'
                    });
                    break;
                case 3:
                    $.message({
                        message:title,
                        type:'info'
                    });
                    break;
                default:
            		$.message({
            			type:'success',
            			message:'<div style="color:#333;font-weight:bold;font-size:16px;">用户信息保存成功<div><span style="color:lightgrey;font-size:small;">'+title+'</span>',
            			duration:3000,
            			center:true
            		})
            }
    }
</script>
<script src="/static/admin/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->


<script src="/static/admin/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['layer', 'miniTab','echarts','element',], function () {
        var $ = layui.jquery,
            layer = layui.layer,
            miniTab = layui.miniTab,
            echarts = layui.echarts,
            element = layui.element;

        miniTab.listen();

       //Hash地址的定位
       var layid = location.hash.replace(/^#test=/, '');
       element.tabChange('test', layid);
      
       element.on('tab(test)', function(elem){
         location.hash = 'test='+ $(this).attr('lay-id');
       });

        /**
         * 报表功能
         */
        var echartsRecords = echarts.init(document.getElementById('echarts-records'), 'walden');
        var optionRecords = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name:'邮件营销',
                    type:'line',
                    data:[120, 132, 101, 134, 90, 230, 210]
                },
                {
                    name:'联盟广告',
                    type:'line',
                    data:[220, 182, 191, 234, 290, 330, 310]
                },
                {
                    name:'视频广告',
                    type:'line',
                    data:[150, 232, 201, 154, 190, 330, 410]
                },
                {
                    name:'直接访问',
                    type:'line',
                    data:[320, 332, 301, 334, 390, 330, 320]
                },
                {
                    name:'搜索引擎',
                    type:'line',
                    data:[820, 932, 901, 934, 1290, 1330, 1320]
                }
            ]
        };
        echartsRecords.setOption(optionRecords);

        // echarts 窗口缩放自适应
        window.onresize = function(){
            echartsRecords.resize();
        }

    });
</script>



</body>


<div id="zsmb" style="background-color:#3f3f3f;padding:20px 30px;width:180px;height:100px;color:white;display:none;">//请点击任意标题查看信息</div>
<script>
    const zsmb = '<?php echo htmlentities(config('app.zsmb')); ?>';
     layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;           
           if(zsmb){
                layer.open({
                    type:1
                    ,title:'助手面板'
                    ,offset:'rt'
                    ,id:'zs'
                    ,content: $('#zsmb')
                    ,shade:0
                    ,yes:function(){
                        layer.closeAll();
                    }
                    ,cancel:function(){
                        //销毁后回调
                    }
                })
            }
     })

    function show_error_msg(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：show_error_msg() <br> 当前显示：显示错误');
    }
    
    function close_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：close() <br> 当前显示：系统日志 <br> <sub>不建议开启，用于开发模式下查看错误日志</sub>');
    }
    
    function zsmb_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：zsmb()'); 
    }
    
    function ZSMB(key,value){
        $('#zsmb').html('配置标识：' + key + '<br> 当前显示：' + value + '<br/><br/>' + '<?php echo htmlentities(config('app.copyright')); ?>'); 
    }

</script>
</html>