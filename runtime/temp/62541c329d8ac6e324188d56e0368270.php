<?php /*a:2:{s:84:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/app/ect_code_page.htm";i:1668502113;s:80:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/public/jquery.htm";i:1651666477;}*/ ?>
<meta name="google" content="notranslate" />
<link rel="stylesheet" href="/static/admin/lib/layui-v2.6.3/css/layui.css" media="all">
<style>
#editor { 
    position: absolute;
    top: 0;
    right: 0;
    bottom: 30px;
    left: 0;
}
#footer{
     
    background-color:#696969;
    color:#ffffff;
    position:absolute;
    bottom:0;
    width:100%;
    height:30px;
}
#footer div{
    padding-top:5px;
}
#footer b{
   
    margin:0px 5px 0px 5px;
    font-size:12px;
}
</style>

<div id="editor"><?php echo htmlentities((isset($file_get_contents) && ($file_get_contents !== '')?$file_get_contents:"")); ?></div>

<div id="footer">
    
    <?php if($file_path != ''): ?>
    <div>
    <b>文件位置：<?php echo htmlentities((isset($file_path) && ($file_path !== '')?$file_path:'')); ?></b>
    <b>语言：<?php echo htmlentities((isset($file_1) && ($file_1 !== '')?$file_1:'php')); ?></b>
    </div>
    <?php endif; ?>
</div>


<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/static/admin/js/message.js"></script>
<script>
    function ectmsg(title,code){

            switch(code)
            {
                case 0:
                    $.message({
                        message:title,
                        type:'success'
                    });
                    break;
                case 1:
                    $.message({
                        message:title,
                        type:'error'
                    });
                    break;
                case 2:
                    $.message({
                        message:title,
                        type:'warning'
                    });
                    break;
                case 3:
                    $.message({
                        message:title,
                        type:'info'
                    });
                    break;
                default:
            		$.message({
            			type:'success',
            			message:'<div style="color:#333;font-weight:bold;font-size:16px;">用户信息保存成功<div><span style="color:lightgrey;font-size:small;">'+title+'</span>',
            			duration:3000,
            			center:true
            		})
            }
    }
</script>
<script src="/static/admin/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="/static/code/src/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="/static/code/src/ext-language_tools.js?v=110"></script>
<!--<script src="/static/code/ace-builds/src/ace.js" type="text/javascript" charset="utf-8"></script>-->
<script>
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.session.setMode("ace/mode/<?php echo htmlentities((isset($file_1) && ($file_1 !== '')?$file_1:'php')); ?>");
    editor.setOptions({
        enableBasicAutocompletion: true,//
        enableSnippets: true,//
        enableLiveAutocompletion: true,// 补全
    });


    //var file = editor.getValue();
    var file = '';
    var type = 1;
    var ectool_name = '<?php echo htmlentities((isset($route) && ($route !== '')?$route:"")); ?>';
    
    var directory_name = parent.directory_name;
    
    editor.on('change', function() {
        type = 0;
        code_hidden_input = document.querySelector('#editor');
        code_hidden_input.value = editor.getValue();
        //console.log(editor.getValue())
        file = editor.getValue();
        //<span class="layui-badge-dot layui-bg-orange"></span>
       
        
    })
    
    //设置自动提示代码
    var setCompleteData = function(data) {
        var langTools = ace.require("ace/ext/language_tools");
        langTools.addCompleter({
            getCompletions: function(editor, session, pos, prefix, callback) {
                if (prefix.length === 0) {
                    return callback(null, []);
                } else {
                    return callback(null, data);
                }
            }
        });
    }
    var data = [{meta: "查询多个数据（数据集）", caption: "query_more", value: "query_more()", score:9999999},{meta: "查询单个数据（FIND）", caption: "query_one", value: "query_one()", score:9999999}];
    setCompleteData(data);
    
    // 保存代码
    editor.commands.addCommand({
    name: 'Commands',
    bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
    exec: function(editor) {
        public_add_file()
    },
        readOnly: true, // false if this command should not apply in readOnly mode
        // multiSelectAction: "forEach", optional way to control behavior with multiple cursors
        // scrollIntoView: "cursor", control how cursor is scolled into view after the command
    });
    
    
    // 访问接口
    editor.commands.addCommand({
    name: 'Commandj',
    bindKey: {win: 'Ctrl-J',  mac: 'Command-J'},
    exec: function(editor) {
        public_jump_function()
    },
        readOnly: true, // false if this command should not apply in readOnly mode
        // multiSelectAction: "forEach", optional way to control behavior with multiple cursors
        // scrollIntoView: "cursor", control how cursor is scolled into view after the command
    });  
        
    function add_file(){
    
        public_add_file();
    }
    
    function load_frame(){
        window.location.reload();
    }
    //console.log("<?php echo htmlentities((isset($file) && ($file !== '')?$file:'')); ?>");
    if("<?php echo htmlentities((isset($file) && ($file !== '')?$file:'')); ?>" == 'config.json' || "<?php echo htmlentities((isset($file) && ($file !== '')?$file:'')); ?>" == 'Mysql.php'){
      $('#delete', window.parent.document).attr('style','display:none;');    
    }else{
      $('#delete', window.parent.document).attr('style','color:#ffffff');   
    }
    
    if("<?php echo htmlentities((isset($file_1) && ($file_1 !== '')?$file_1:'php')); ?>" !== 'php' || "<?php echo htmlentities((isset($file) && ($file !== '')?$file:'')); ?>" == 'Mysql.php'){
      $('#jump_function', window.parent.document).attr('style','display:none;');  
    }else{
       $('#jump_function', window.parent.document).attr('style','color:#ffffff');  
    }
    
    function delete_file(){
        var file_path = "<?php echo htmlentities((isset($file_path) && ($file_path !== '')?$file_path:'')); ?>";
        public_delete(file_path);
    }
    
    function jump_function(){
    
        public_jump_function();
        
    }
    
    function open_web(url){
        parent.open_web(url);
    }
    
    
    function public_jump_function(){
        editor.getSelectedText(); // or for a specific range
        var function_name = editor.session.getTextRange(editor.getSelectionRange()); 
        if(function_name == ''){
            layer.msg('请先选择方法名哦');
            return;
        }
        
 
        var index = layer.open({
            type:1
            ,tipsMore: true
            ,title:false
            ,closeBtn:0
            ,offset:'rt'
            ,id:'zs'
            ,content: '<div style="background-color:#3f3f3f;padding:20px 30px;width:180px;height:100px;color:white;font-size:12px;"><b id="jf_return"></b></div>'
            ,shade:0
            ,yes:function(res){
               // layer.closeAll();
            }
        })
        
        $('#jf_return').append('<br><b id="jf_return">发送请求。。。</b>')
        
        $.get('/EctAdminApi/jump_function',{'function_name':function_name,'ectool_name':ectool_name,'directory_name':directory_name},function(res){
            if(res>0){
                $('#jf_return').append('<br><b id="jf_return">'+res+'</b>')
            //console.log(res);
            }else{
                $('#jf_return').append('<br><b id="jf_return">'+res.msg+'</b>')
                setTimeout(function(){layer.close(index)},3000);
                //value
                //window.open('http://' + res.value);
                // layer.msg('http://' + res.value)
                if(<?php echo htmlentities($ssl); ?> == 0){
                    $('#jf_return').append('<br>https://' + res.value)
                    open_web('https://' + res.value)
                }else{
                    $('#jf_return').append('<br>http://' + res.value)
                    open_web('http://' + res.value)
                }
                //layer.msg(res.value)
            }
        },'json')
    }
    
    function public_add_file(){
        if(type>0){layer.msg('您未修改');return;}
        $('#add_file', window.parent.document).attr('class','layui-icon layui-icon-loading layui-font-12')
        layer.msg('保存中.....');
        $.post('/EctAdminApi/add_file',{'data':file,'file_path':'<?php echo htmlentities((isset($file_path) && ($file_path !== '')?$file_path:"")); ?>'},function(res){
            if(res.code>0){
                layer.msg(res.msg);
                $('#add_file', window.parent.document).attr('class','layui-icon layui-icon-radio layui-font-12')
            }else{
                layer.msg(res.msg);
                type = 1;
                $('#add_file', window.parent.document).attr('class','layui-icon layui-icon-radio layui-font-12')
            }
        },'json');
    }
    
    function public_delete(file_path){
        
        layer.confirm('确定删除吗？', {
            btn: ['确定', '取消'] //可以无限个按钮
              ,btn3: function(index, layero){
                //按钮【按钮三】的回调
              }
            }, function(index, layero){
              //按钮【按钮一】的回调
              $.get('/EctAdminApi/delete_file',{file_path:file_path},function(res){
                if(res.code>0){
                    layer.msg(res.msg);
                }else{
                    layer.msg(res.msg);
                    setTimeout(function(){parent.reloads()},1000);
                    $('#code').attr("src",''); 
                }                  
              },'json');
            }, function(index){
              //按钮【按钮二】的回调
        });
        
    }
</script>