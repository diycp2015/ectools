<?php /*a:3:{s:78:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/set/set_web.htm";i:1668016769;s:79:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/extends/Base.htm";i:1668502636;s:80:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/public/jquery.htm";i:1651666477;}*/ ?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title>网站配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/css/public.css" media="all">

    <style>
    input{color:#76838f;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>前端配置</legend>
</fieldset>
<form class="layui-form" action="">
    <div class="layui-form-item">
        <label class="layui-form-label" onclick="ZSMB('web_title','网站标题')">网站标题</label>
        <div class="layui-input-inline">
            <input type="text" name="web_title" placeholder="请输入网站标题" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['web_title']); ?>">
        </div>
    </div>
    
    <div class="layui-form-item">
        <label class="layui-form-label" onclick="ZSMB('web_logo','网站logo')">网站logo</label>
        <div class="layui-upload-drag" id="logo">
          <i class="layui-icon"></i>
          <p>上传Logo</p>
          <div class="layui-hide" id="uploadlogo">
            <hr>
            <img src="" alt="上传成功后渲染" style="max-width: 196px">
          </div>
        </div>
        <img id="sl_logo" type="<?php echo !empty($data['web_logo']) ? '' : 'hidden'; ?>" src="<?php echo htmlentities($data['web_logo']); ?>" style="max-width: 80px ;max-height:80px;">
    </div>
    
    <div class="layui-form-item">
        <label class="layui-form-label" onclick="ZSMB('web_seo_key','seo关键词')">seo关键词</label>
        <div class="layui-input-block">
            <input type="text" name="web_seo_key" placeholder="请输入seo关键词" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['web_seo_key']); ?>">
         <sub>逗号 ， 隔开</sub>
        </div>
    </div>
    
    <div class="layui-form-item">
        <label class="layui-form-label" onclick="ZSMB('web_seo_desc','seo介绍')">seo介绍</label>
        <div class="layui-input-block">
            <input type="text" name="web_seo_desc" placeholder="请输入seo介绍" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['web_seo_desc']); ?>">
        </div>
    </div>
    
    <div class="layui-form-item">
        <label class="layui-form-label" onclick="ZSMB('web_copyright','版权所有')">版权所有</label>
        <div class="layui-input-block">
            <input type="text" name="web_copyright" placeholder="请输入版权所有" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['web_copyright']); ?>">
        </div>
    </div>
    
    <div class="layui-form-item">
        <label class="layui-form-label" onclick="ZSMB('web_icp','网站备案号')">网站备案号</label>
        <div class="layui-input-inline">
            <input type="text" name="web_icp" placeholder="请输入网站备案号" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['web_icp']); ?>">
        </div>
    </div>
    
    <input type="hidden" name="web_logo" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['web_logo']); ?>">
    
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn layui-btn-normal" lay-submit="" lay-filter="save">保存</button>
        </div>
    </div>
</form>

    </div>
</div>
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/static/admin/js/message.js"></script>
<script>
    function ectmsg(title,code){

            switch(code)
            {
                case 0:
                    $.message({
                        message:title,
                        type:'success'
                    });
                    break;
                case 1:
                    $.message({
                        message:title,
                        type:'error'
                    });
                    break;
                case 2:
                    $.message({
                        message:title,
                        type:'warning'
                    });
                    break;
                case 3:
                    $.message({
                        message:title,
                        type:'info'
                    });
                    break;
                default:
            		$.message({
            			type:'success',
            			message:'<div style="color:#333;font-weight:bold;font-size:16px;">用户信息保存成功<div><span style="color:lightgrey;font-size:small;">'+title+'</span>',
            			duration:3000,
            			center:true
            		})
            }
    }
</script>
<script src="/static/admin/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->


<script>
    layui.use(['form', 'layedit', 'laydate', 'element', 'upload'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate
            , element = layui.element
            , upload= layui.upload;

        //日期
        laydate.render({
            elem: '#date'
        });
        laydate.render({
            elem: '#date1'
        });

        //创建一个编辑器
        var editIndex = layedit.build('LAY_demo_editor');

        //自定义验证规则
        form.verify({
            title: function (value) {
                if (value.length < 5) {
                    return '标题至少得5个字符啊';
                }
            }
            , pass: [
                /^[\S]{6,12}$/
                , '密码必须6到12位，且不能出现空格'
            ]
            , content: function (value) {
                layedit.sync(editIndex);
            }
        });

        //监听提交
        form.on('submit(save)', function (data) {
            $_data = JSON.stringify(data.field);
            //console.log(data.field);
            
            $.post('/EctAdminApi/set_web',{web_title:data.field.web_title,web_logo:data.field.web_logo,web_seo_key:data.field.web_seo_key,web_seo_desc:data.field.web_seo_desc,web_copyright:data.field.web_copyright,web_icp:data.field.web_icp},function(res){
                if(res.code>0){
                    layer.msg(res.msg);
                }else{
                    layer.msg(res.msg);
                }
            },'json');
            
            return false;
            
        });

        //表单初始赋值
        form.val('example', {
            "username": "贤心" // "name": "value"
            , "password": "123456"
            , "interest": 1
            , "like[write]": true //复选框选中状态
            , "close": true //开关状态
            , "sex": "女"
            , "desc": "我爱 layui"
        })
        
      //拖拽上传
      upload.render({
        elem: '#logo'
        ,url: '/EctAdminApi/upload_logo'
        ,done: function(res){
            console.log(res)
          layer.msg(res.msg);
          layui.$('#uploadlogo').removeClass('layui-hide').find('img').attr('src', res.value);
          $('input[name="web_logo"]').val(res.value);
          $('#sl_logo').val(res.value);
        }
      });

    });
</script>



</body>


<div id="zsmb" style="background-color:#3f3f3f;padding:20px 30px;width:180px;height:100px;color:white;display:none;">//请点击任意标题查看信息</div>
<script>
    const zsmb = '<?php echo htmlentities(config('app.zsmb')); ?>';
     layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;           
           if(zsmb){
                layer.open({
                    type:1
                    ,title:'助手面板'
                    ,offset:'rt'
                    ,id:'zs'
                    ,content: $('#zsmb')
                    ,shade:0
                    ,yes:function(){
                        layer.closeAll();
                    }
                    ,cancel:function(){
                        //销毁后回调
                    }
                })
            }
     })

    function show_error_msg(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：show_error_msg() <br> 当前显示：显示错误');
    }
    
    function close_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：close() <br> 当前显示：系统日志 <br> <sub>不建议开启，用于开发模式下查看错误日志</sub>');
    }
    
    function zsmb_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：zsmb()'); 
    }
    
    function ZSMB(key,value){
        $('#zsmb').html('配置标识：' + key + '<br> 当前显示：' + value + '<br/><br/>' + '<?php echo htmlentities(config('app.copyright')); ?>'); 
    }

</script>
</html>