<?php /*a:3:{s:72:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/log/ect_log.htm";i:1647625565;s:73:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/extends/Base.htm";i:1644397262;s:74:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/public/jquery.htm";i:1651666477;}*/ ?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title>操作日志</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/css/public.css" media="all">
    <style>
    input{color:#76838f;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">关键词</label>
                            <div class="layui-input-inline">
                                <input type="text" name="username" autocomplete="off" class="layui-input">
                            </div>
                        </div>

                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>
        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="delete"> 批量删除 </button>
            </div>
            <?php if(config('app.ect_log') == ''): ?>
             <b style="color:red;">操作日志已关闭《请先前往 网站设置-系统配置 打开，才会进行记录》</b>
            <?php endif; ?>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>


        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="look_log">查看</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

    </div>
</div>
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/static/admin/js/message.js"></script>
<script>
    function ectmsg(title,code){

            switch(code)
            {
                case 0:
                    $.message({
                        message:title,
                        type:'success'
                    });
                    break;
                case 1:
                    $.message({
                        message:title,
                        type:'error'
                    });
                    break;
                case 2:
                    $.message({
                        message:title,
                        type:'warning'
                    });
                    break;
                case 3:
                    $.message({
                        message:title,
                        type:'info'
                    });
                    break;
                default:
            		$.message({
            			type:'success',
            			message:'<div style="color:#333;font-weight:bold;font-size:16px;">用户信息保存成功<div><span style="color:lightgrey;font-size:small;">'+title+'</span>',
            			duration:3000,
            			center:true
            		})
            }
    }
</script>
<script src="/static/admin/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->


<script>
    layui.use(['form', 'table', 'layer'], function () {
        var $ = layui.jquery,
            layer = layui.layer,
            form = layui.form,
            table = layui.table;

        table.render({
            elem: '#currentTableId',
            url: '/EctAdminJson/ect_log',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID', sort: true},
                // {field: 'name', width: 180, title: '请求名'},
                {field: 'adminId', width: 80, title: '用户', sort: true},
                {field: 'url', width: 180, title: '请求接口'},
                {field: 'place', width: 190, title: 'IP地址', templet:place, sort: true},
                {field: 'remark', width: 120, title: '请求方式'},
                {field: 'time', width: 180, title: '请求日期',templet:time},
                {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });
        
        function place(n){
            return n.place + '/<b>' + n.place_zh + '</b>';
        }
        
        function time(n){
           return getLocalTime(n.time);
        }
        function getLocalTime(nS) {  
         return new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/,' ');  
        }


        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            var result = JSON.stringify(data.field);
            layer.alert(result, {
                title: '最终的搜索信息'
            });

            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: {
                    searchParams: result
                }
            }, 'data');

            return false;
        });

        /**
         * toolbar监听事件
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {  // 监听添加操作
                var index = layer.open({
                    title: '添加用户',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: '../page/table/add.html',
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {  // 
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                if(data.length<1){
                    layer.msg('您未选择任何');
                    return;
                }
                
                var _data = JSON.stringify(data);
                $.post('/EctAdminApi/delete_ect_log',{'data':_data},function(res){
                    
                    if(res.code>0){
                        layer.msg(res.msg);
                    }else{
                        layer.msg(res.msg);
                        setTimeout(function(){window.location.reload()},1000);
                    }
                },'json');
                //console.log(data);
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            //console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            var id = obj.data.id
            if (obj.event === 'look_log') {
                
                var index = layer.open({
                    title: '#' + id + '&nbsp;&nbsp;日志详情页',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    resizing:true,
                    shadeClose: true,
                    area: ['40%', '60%'],
                    content: '/ect_log_page?id=' + id,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                //console.log(obj)
                layer.confirm('删除后将无法恢复哦！', function (index) {
                    
                    $.get('/EctAdminApi/delete_ect_log',{'id':id},function(res){
                        if(res.code>0){
                            layer.msg(res.msg);
                        }else{
                            layer.msg(res.msg); 
                            obj.del();
                            layer.close(index);
                        }
                    },'json');
                    
                    //console.log(index);
                });
            }
        });

    });
</script>



</body>


<div id="zsmb" style="background-color:#3f3f3f;padding:20px 30px;width:180px;height:100px;color:white;display:none;">//请点击任意标题查看信息</div>
<script>
    const zsmb = '<?php echo htmlentities(config('app.zsmb')); ?>';
     layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;           
           if(zsmb){
                layer.open({
                    type:1
                    ,title:'助手面板'
                    ,offset:'rt'
                    ,id:'zs'
                    ,content: $('#zsmb')
                    ,shade:0
                    ,yes:function(){
                        layer.closeAll();
                    }
                    ,cancel:function(){
                        //销毁后回调
                    }
                })
            }
     })

    function show_error_msg(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：show_error_msg() <br> 当前显示：显示错误');
    }
    
    function close_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：close() <br> 当前显示：系统日志 <br> <sub>不建议开启，用于开发模式下查看错误日志</sub>');
    }
    
    function zsmb_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：zsmb()'); 
    }
    
    function ZSMB(key,value){
        $('#zsmb').html('配置标识：' + key + '<br> 当前显示：' + value + '<br/><br/>' + '<?php echo htmlentities(config('app.copyright')); ?>'); 
    }

</script>
</html>