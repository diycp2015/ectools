<?php /*a:3:{s:81:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/info/ect_system_info.htm";i:1661742184;s:73:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/extends/Base.htm";i:1644397262;s:74:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/ect_admin/public/jquery.htm";i:1651666477;}*/ ?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/css/public.css" media="all">
    <style>
    input{color:#76838f;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        
  <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-row layui-col-space15">
                    
                    <div class="layui-col-md8">
                        <div class="layui-card">
                            <div class="layui-card-header"><i class="layui-icon">&#xe635;</i> 环境信息</div>
                            <div class="layui-card-body layui-text">
                                <table class="layui-table">
                                    <colgroup>
                                        <col width="100">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <td><b onclick="ZSMB('PHP_VERSION','PHP版本')">PHP版本</b></td>
                                        <td>
                                            <?php echo htmlentities($system_info['PHP_VERSION']); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b onclick="ZSMB('MYSQL_VERSION','Mysql版本')">Mysql版本</b></td>
                                        <td><?php echo htmlentities($system_info['MYSQL_VERSION']); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b onclick="ZSMB('PROCESSOR_IDENTIFIER','CPU数量')">CPU数量</b></td>
                                        <td><?php echo htmlentities($system_info['PROCESSOR_IDENTIFIER']); ?></td>
                                    </tr>       
                                    <tr>
                                        <td><b onclick="ZSMB('SERVER_SOFTWARE','解析引擎')">解析引擎</b></td>
                                        <td><?php echo htmlentities($system_info['SERVER_SOFTWARE']); ?></td>
                                    </tr>  
                                    <tr>
                                        <td><b onclick="ZSMB('HTTP_ACCEPT_LANGUAGE','服务器语言')">服务器语言</b></td>
                                        <td><?php echo htmlentities($system_info['HTTP_ACCEPT_LANGUAGE']); ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b onclick="ZSMB('SERVER_PORT','Web端口')">Web端口</b></td>
                                        <td><?php echo htmlentities($system_info['SERVER_PORT']); ?></td>
                                    </tr>           
                                    <tr>
                                        <td><b onclick="ZSMB('php_uname','系统信息')">系统信息</b></td>
                                        <td><?php echo htmlentities($system_info['php_uname']); ?></td>
                                    </tr>         
                                    <tr>
                                        <td><b onclick="ZSMB('HTTP_USER_AGENT','客户端信息')">客户端信息</b></td>
                                        <td>
                                            <?php echo htmlentities($system_info['HTTP_USER_AGENT']); ?>
                                        </td>
                                    </tr>
                                    
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    <div class="layui-col-md4">
    
                       <div class="layui-card">
                            <div class="layui-card-header"><i class="layui-icon">&#xe674;</i> 系统信息</div>
                            <div class="layui-card-body layui-text">
                                <table class="layui-table">
                                    <colgroup>
                                        <col width="100">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <td>框架名称</td>
                                        <td>
                                            Ec tools
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>当前版本</td>
                                        <td>v1.0.0</td>
                                    </tr>
                                    <tr>
                                        <td>主要特色</td>
                                        <td>APP / 小程序 / Web / 快速开发框架</td>
                                    </tr>
                                    <tr>
                                        <td>演示地址</td>
                                        <td>
                                            v1：<a href="l" target="_blank">内测阶段</a><br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>下载地址</td>
                                        <td>
                                            v1：<a href="" target="_blank">内测阶段</a> / <a href="" target="_blank">内测阶段</a><br>
 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>后台界面Ui采用layuimini</td>
                                        <td style="padding-bottom: 0;">
                                            <div class="layui-btn-container">
                                                <a href="https://gitee.com/zhongshaofa/layuimini" target="_blank" style="margin-right: 15px"><img src="https://gitee.com/zhongshaofa/layuimini/badge/star.svg?theme=dark" alt="star"></a>
                                                <a href="https://gitee.com/zhongshaofa/layuimini" target="_blank"><img src="https://gitee.com/zhongshaofa/layuimini/badge/fork.svg?theme=dark" alt="fork"></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <!--<tr>-->
                                    <!--    <td>Github</td>-->
                                    <!--    <td style="padding-bottom: 0;">-->
                                    <!--        <div class="layui-btn-container">-->
                                    <!--            <iframe src="https://ghbtns.com/github-btn.html?user=zhongshaofa&repo=layuimini&type=star&count=true" frameborder="0" scrolling="0" width="100px" height="20px"></iframe>-->
                                    <!--            <iframe src="https://ghbtns.com/github-btn.html?user=zhongshaofa&repo=layuimini&type=fork&count=true" frameborder="0" scrolling="0" width="100px" height="20px"></iframe>-->
                                    <!--        </div>-->
                                    <!--    </td>-->
                                    <!--</tr>-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    <div class="layui-col-md12">
                       <div class="layui-card">
                            <div class="layui-card-header"><i class="layui-icon">&#xe67f;</i> PHP信息</div>
                            <div class="layui-card-body layui-text">
                                <table class="layui-table">
                                    <colgroup>
                                        <col width="100">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <td><b onclick="ZSMB('php_sapi_name','PHP运行方式')">PHP运行方式</b></td>
                                        <td>
                                            <?php echo htmlentities($system_info['php_sapi_name']); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b onclick="ZSMB('safe_mode','PHP安全模式')">PHP安全模式</b></td>
                                        <td>
                                            <?php echo htmlentities($system_info['safe_mode']); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b onclick="ZSMB('disable_functions','禁用函数')">禁用函数</b></td>
                                        <td><?php if(is_array($system_info['disable_functions']) || $system_info['disable_functions'] instanceof \think\Collection || $system_info['disable_functions'] instanceof \think\Paginator): $i = 0; $__LIST__ = $system_info['disable_functions'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?> <span class="layui-badge layui-bg-black"><?php echo htmlentities($vo); ?></span> <?php endforeach; endif; else: echo "" ;endif; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b onclick="ZSMB('php_loaded_extensions','已加载扩展库')">已加载扩展库</b></td>
                                        <td><?php if(is_array($system_info['php_loaded_extensions']) || $system_info['php_loaded_extensions'] instanceof \think\Collection || $system_info['php_loaded_extensions'] instanceof \think\Paginator): $i = 0; $__LIST__ = $system_info['php_loaded_extensions'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?> <span class="layui-badge layui-bg-blue"><?php echo htmlentities($vo); ?></span> <?php endforeach; endif; else: echo "" ;endif; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b onclick="ZSMB('upload_max_filesize','PHP上传限制')">PHP上传限制</b></td>
                                        <td><?php echo htmlentities($system_info['upload_max_filesize']); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b onclick="ZSMB('max_execution_time','脚本超时时间')">脚本超时时间</b></td>
                                        <td><?php echo htmlentities($system_info['max_execution_time']); ?></td>
                                    </tr> 
                                    <tr>
                                    <td><b onclick="ZSMB('post_max_size','POST提交大小')">POST提交大小</b></td>
                                        <td><?php echo htmlentities($system_info['post_max_size']); ?></td>
                                    </tr>                                               
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

    </div>

    </div>
</div>
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/static/admin/js/message.js"></script>
<script>
    function ectmsg(title,code){

            switch(code)
            {
                case 0:
                    $.message({
                        message:title,
                        type:'success'
                    });
                    break;
                case 1:
                    $.message({
                        message:title,
                        type:'error'
                    });
                    break;
                case 2:
                    $.message({
                        message:title,
                        type:'warning'
                    });
                    break;
                case 3:
                    $.message({
                        message:title,
                        type:'info'
                    });
                    break;
                default:
            		$.message({
            			type:'success',
            			message:'<div style="color:#333;font-weight:bold;font-size:16px;">用户信息保存成功<div><span style="color:lightgrey;font-size:small;">'+title+'</span>',
            			duration:3000,
            			center:true
            		})
            }
    }
</script>
<script src="/static/admin/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->


<script>

</script>



</body>


<div id="zsmb" style="background-color:#3f3f3f;padding:20px 30px;width:180px;height:100px;color:white;display:none;">//请点击任意标题查看信息</div>
<script>
    const zsmb = '<?php echo htmlentities(config('app.zsmb')); ?>';
     layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;           
           if(zsmb){
                layer.open({
                    type:1
                    ,title:'助手面板'
                    ,offset:'rt'
                    ,id:'zs'
                    ,content: $('#zsmb')
                    ,shade:0
                    ,yes:function(){
                        layer.closeAll();
                    }
                    ,cancel:function(){
                        //销毁后回调
                    }
                })
            }
     })

    function show_error_msg(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：show_error_msg() <br> 当前显示：显示错误');
    }
    
    function close_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：close() <br> 当前显示：系统日志 <br> <sub>不建议开启，用于开发模式下查看错误日志</sub>');
    }
    
    function zsmb_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：zsmb()'); 
    }
    
    function ZSMB(key,value){
        $('#zsmb').html('配置标识：' + key + '<br> 当前显示：' + value + '<br/><br/>' + '<?php echo htmlentities(config('app.copyright')); ?>'); 
    }

</script>
</html>